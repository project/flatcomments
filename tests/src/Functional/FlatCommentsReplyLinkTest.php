<?php

namespace Drupal\Tests\flat_comments\Functional;

use Drupal\comment\CommentManagerInterface;
use Drupal\comment\Tests\CommentTestTrait;
use Drupal\field\Entity\FieldConfig;
use Drupal\Tests\comment\Functional\CommentTestBase;

/**
 * Test that the flat_comments module is hiding correctly the "reply" link.
 *
 * @package Drupal\Tests\flat_comments\Functional
 * @group flat_comments
 */
class FlatCommentsReplyLinkTest extends CommentTestBase {
  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'classy';

  use CommentTestTrait;

  /**
   * Modules to install.
   *
   * @var array
   */
  public static $modules = ['flat_comments', 'comment', 'node'];

  /**
   * A test node to which comments will be posted.
   *
   * @var \Drupal\node\NodeInterface
   */
  protected $node;

  /**
   * Tests the comment reply link is hid correctly.
   */
  public function testCommentThreading() {
    // Set comments to have a subject with preview disabled.
    $field = FieldConfig::loadByName('node', 'article', 'comment');
    $field->setSetting('default_mode', CommentManagerInterface::COMMENT_MODE_FLAT);
    $field->setSetting('preview', DRUPAL_DISABLED);
    $field->setSetting('form_location', TRUE);
    $field->setThirdPartySetting('flat_comments', 'remove_reply_link', TRUE);
    $field->save();

    // Create a node.
    $this->drupalLogin($this->adminUser);
    $this->node = $this->drupalCreateNode(
      ['type' => 'article', 'promote' => 1, 'uid' => $this->adminUser->id()]
    );

    // Post comment.
    $this->drupalLogin($this->adminUser);
    $subject_text = $this->randomMachineName();
    $comment_text = $this->randomMachineName();

    $comment1 = $this->postComment(
      $this->node,
      $comment_text,
      $subject_text,
      TRUE
    );
    // Confirm that the comment was created.
    $this->assertTrue(
      $this->commentExists($comment1),
      'Comment found.'
    );
    $this->assertNoLink('Reply');
    $this->drupalLogout();

  }

}
