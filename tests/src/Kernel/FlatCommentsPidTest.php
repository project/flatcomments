<?php

namespace Drupal\Tests\flat_comments\Kernel;

use Drupal\comment\CommentManagerInterface;
use Drupal\comment\Entity\Comment;
use Drupal\comment\Tests\CommentTestTrait;
use Drupal\entity_test\Entity\EntityTest;
use Drupal\field\Entity\FieldConfig;
use Drupal\KernelTests\KernelTestBase;

/**
 * Tests the new entity API for the comment field type.
 *
 * @group comment
 */
class FlatCommentsPidTest extends KernelTestBase {

  use CommentTestTrait;

  /**
   * Modules to enable.
   *
   * @var array
   */
  public static $modules = [
    'comment',
    'system',
    'field',
    'text',
    'flat_comments',
    'entity_test',
    'user',
  ];

  protected function setUp() {
    parent::setUp();
    $this->installEntitySchema('comment');
    $this->installSchema('comment', ['comment_entity_statistics']);
    $this->installConfig(['comment']);
    $this->installEntitySchema('entity_test');
    $this->installEntitySchema('user');
    $this->installSchema('system', ['sequences', 'key_value']);

    // Set default storage backend and configure the theme system.
    $this->installConfig(['field', 'system']);

    // Create user 1.
    $storage = \Drupal::entityTypeManager()->getStorage('user');
    $storage
      ->create([
        'uid' => 1,
        'name' => 'entity-test',
        'mail' => 'entity@localhost',
        'status' => TRUE,
      ])
      ->save();
  }

  /**
   * Tests using entity fields of the comment field type.
   */
  public function testPidIsNull() {
    $this->addDefaultCommentField('entity_test', 'entity_test', 'comment');
    $field = FieldConfig::loadByName('entity_test', 'entity_test', 'comment');
    $field->setSetting('default_mode', CommentManagerInterface::COMMENT_MODE_FLAT);
    $field->save();

    // Verify entity creation.
    $entity = EntityTest::create();
    $entity->name->value = $this->randomMachineName();
    $entity->save();

    // Create some comments.
    $comment = Comment::create([
      'subject' => 'My comment title',
      'uid' => 1,
      'name' => 'entity-test',
      'mail' => 'entity@localhost',
      'entity_type' => 'entity_test',
      'field_name' => 'comment',
      'entity_id' => $entity->id(),
      'comment_type' => 'entity_test',
      'status' => 1,
    ]);
    $comment->save();

    $comment2 = Comment::create([
      'subject' => 'Anonymous comment title',
      'uid' => 1,
      'name' => 'entity-test',
      'mail' => 'entity@localhost',
      'entity_type' => 'entity_test',
      'field_name' => 'comment',
      'entity_id' => $entity->id(),
      'comment_type' => 'entity_test',
      'status' => 1,
      'pid' => $comment->id(),
    ]);
    $comment2->save();

    // Even that explicitly was set a parent comment it be will removed thanks
    // to the flat comments module.
    $this->assertFalse($comment2->hasParentComment());

    // And the thread must be '02/' instead of '01.00/'.
    $this->assertTrue($comment2->getThread() == '02/');
  }

}
